# Metadata

## DataCite

<details>

```json
{
    "title": "DataCite",
    "properties": [
        {
            "name": "identifier",
            "description": "Identifier - A persistent identifier that identifies a resource.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "creators",
            "description": "Creators - The main researchers involved working on the data, or the authors of the publication in priority order. May be a corporate/institutional or personal name. Format: Family, Given. Personal names can be further specified using givenName and familyName.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "titles",
            "description": "Titles - A name or title by which a resource is known.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "publisher",
            "description": "Publisher - The name of the entity that holds, archives, publishes prints, distributes, releases, issues, or produces the resource. This property will be used to formulate the citation, so consider the prominence of the role. In the case of datasets, \"publish\" is understood to mean making the data available to the community of researchers.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "publicationYear",
            "description": "Publication Year - Year when the data is made publicly available. If an embargo period has been in effect, use the date when the embargo period ends. In the case of datasets, \"publish\" is understood to mean making the data available on a specific date to the community of researchers. If there is no standard publication year value, use the date that would be preferred from a citation perspective. YYYY",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "resourceType",
            "description": "Resource Type - The type of a resource. You may enter an additional free text description. The format is open, but the preferred format is a single term of some detail so that a pair can be formed with the sub-property.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "subjects",
            "description": "Subjects - Subject, keywords, classification codes, or key phrases describing the resource.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "contributors",
            "description": "Contributors - The institution or person responsible for collecting, creating, or otherwise contributing to the developement of the dataset. The personal name format should be: Family, Given.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "dates",
            "description": "Dates - Different dates relevant to the work. YYYY,YYYY-MM-DD, YYYY-MM-DDThh:mm:ssTZD or any other format or level of granularity described in W3CDTF. Use RKMS-ISO8601 standard for depicting date ranges.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "language",
            "description": "Language - Primary language of the resource. Allowed values are taken from  IETF BCP 47, ISO 639-1 language codes.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "alternateIdentifiers",
            "description": "Alternate Identifiers - An identifier or identifiers other than the primary Identifier applied to the resource being registered. This may be any alphanumeric string which is unique within its domain of issue. May be used for local identifiers. AlternateIdentifier should be used for another identifier of the same instance (same location, same file).",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "relatedIdentifiers",
            "description": "Related Identifiers - Identifiers of related resources. Use this property to indicate subsets of properties, as appropriate.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "sizes",
            "description": "Sizes - Unstructures size information about the resource.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "formats",
            "description": "Formats - Technical format of the resource. Use file extension or MIME type where possible.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "version",
            "description": "Version - Version number of the resource. If the primary resource has changed the version number increases. Register a new identifier for a major version change. Individual stewards need to determine which are major vs. minor versions. May be used in conjunction with properties 11 and 12 (AlternateIdentifier and RelatedIdentifier) to indicate various information updates. May be used in conjunction with property 17 (Description) to indicate the nature and file/record range of version.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "rightsList",
            "description": "Rights List - Any rights information for this resource. Provide a rights management statement for the resource or reference a service providing such information. Include embargo information if applicable.\nUse the complete title of a license and include version information if applicable.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "descriptions",
            "description": "Descriptions - All additional information that does not fit in any of the other categories. May be used for technical information. It is a best practice to supply a description.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "geoLocations",
            "description": "Geo Locations - Spatial region or named place where the data was gathered or about which the data is focused.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "fundingReferences",
            "description": "Funding References - Information about financial support (funding) for the resource being registered.",
            "default": "",
            "optional": true,
            "vocabulary": []
        },
        {
            "name": "relatedItems",
            "description": "Related Items - Information about a resource related to the one being registered e.g. a journal or book of which the article or chapter is part.",
            "default": "",
            "optional": true,
            "vocabulary": []
        }
    ]
}
```
</details>

## Dublin Core

<details>

```json
{
    "title": "Dublin Core",
    "description": "Dublin Core\u2122 Metadata Element Set",
    "properties": {
        "format": {
            "description": "The file format, physical medium, or dimensions of the resource.",
            "optional": "True"
        },
        "type": {
            "description": "The nature or genre of the resource.",
            "optional": "True"
        },
        "language": {
            "description": "A language of the resource.",
            "optional": "True"
        },
        "title": {
            "description": "A name given to the resource.",
            "optional": "True"
        },
        "subject": {
            "description": "The topic of the resource.",
            "optional": "True"
        },
        "coverage": {
            "description": "The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant.",
            "optional": "True"
        },
        "description": {
            "description": "An account of the resource.",
            "optional": "True"
        },
        "creator": {
            "description": "An entity primarily responsible for making the resource.",
            "default": "getUsername",
            "optional": "True"
        },
        "publisher": {
            "description": "An entity responsible for making the resource available.",
            "optional": "True"
        },
        "contributor": {
            "description": "An entity responsible for making contributions to the resource.",
            "optional": "True"
        },
        "rights": {
            "description": "Information about rights held in and over the resource.",
            "optional": "True"
        },
        "source": {
            "description": "A related resource from which the described resource is derived.",
            "optional": "True"
        },
        "relation": {
            "description": "A related resource.",
            "optional": "True"
        },
        "date": {
            "description": "A point or period of time associated with an event in the lifecycle of the resource.",
            "default": "todaysDate",
            "optional": "True"
        }
    }
}
```
</details>

## MMD

<details>

```json
{
    "title": "MMD Simple",
    "description": "MPCDF Metadata Format",
    "properties": {
        "Username": {
            "description": "Your MPCDF user name",
            "default": "getUsername",
            "optional": "True"
        },
        "Institute": {
            "description": "Your institute",
            "optional": "True",
            "vocabulary": [
                "MPCDF",
                "MPX",
                "MPY",
                "MPZ"
            ]
        },
        "Department": {
            "description": "Your department",
            "optional": "True"
        },
        "Group": {
            "description": "Your working group",
            "optional": "True"
        },
        "Project": {
            "description": "The project the data belongs to",
            "optional": "True"
        },
        "Contributors": {
            "description": "Further contributors to the data",
            "optional": "True"
        },
        "Date": {
            "description": "Current date",
            "default": "todaysDate",
            "optional": "True"
        },
        "Name": {
            "description": "The name of the dataset",
            "optional": "True"
        },
        "Description": {
            "description": "A short description",
            "optional": "True"
        },
        "Homepage": {
            "description": "The internet home of the data, respectively the project it belongs to",
            "optional": "True"
        },
        "Target": {
            "description": "What is described with this metadata (a file, a folder etc.)",
            "optional": "True"
        },
        "Storage": {
            "description": "On which MPCDF storage system the data is stored",
            "optional": "True",
            "vocabulary": [
                "HPSS",
                "GHIx",
                "Nexus Posix",
                "Nexus S3",
                "DataShare",
                "Gitlab"
            ]
        },
        "Persistent": {
            "description": "Persistence status of the data (backup, yes/no etc.)",
            "default": "y",
            "optional": "True",
            "vocabulary": [
                "y",
                "n"
            ]
        },
        "Format": {
            "description": "Format of the data",
            "optional": "True"
        },
        "Size": {
            "description": "Size of the data in bytes",
            "optional": "True"
        },
        "Comment": {
            "description": "Any further comments",
            "optional": "True"
        }
    }
}
```
</details>
